‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
                                 Welcome to the
                          System Crafters Newsletter!

                           Issue #007 - March 1, 2024

                                by David Wilson


         --  This newsletter is best viewed with a monospace font!   --
         --    If your e-mail client can't do that, use this URL:    --
         --  https://systemcrafters.net/newsletter/sc-news-007.html  --
         --            Read it in Emacs with `M-x eww`!              --

== Contents ==

1. Introduction
2. Plain Org Files or Zettelkästen-style?
3. Join the March iteration of Hands-On Guile Scheme for Beginners!
4. Crafter News
5. Friday's Stream - Crafting a Minimal Writing Environment in Emacs
6. Closing

== Introduction ==

Welcome to the new issue of the System Crafters Newsletter!

The big news this week is that the next iteration fo the Hands-On Guile Scheme
for Beginners course is now open for registration!  See the section below for
more details.

Now, here's a topic I'd love to hear your feedback on!

== Plain Org Files or Zettelkästen-style? ==

One thing I've been thinking about a lot recently is whether Zettelkästen-style
notes are the best way to organize your important projects and information.

A trick I devised back when I was using Org Roam was to create notes that were
tagged as "project" so that I can find them all and add them to my Org Agenda.
You can watch this video if you haven't seen it:

  -> https://www.youtube.com/watch?v=CUkuyW6hr18

This works great for quickly creating new project notes and having their tasks
show up in your agenda views, but I've actually found that it makes it harder
for me to keep a mental map of all projects I've got going on.

These days I use Denote for all of my notes and project tracking.  Sometimes I
feel like I'm totally losing sight of what information is stored in all of my
note files!

Is it possible that a plain folder of Org files (with an organized hierarchy) is
better?  I feel like it would make your set of projects faster to look over and
review because you just look at the files contained within your notes folder.

On the other hand, perhaps it's possible that I'm just not using "meta notes"
effectively.  A meta note enables you to aggregate links to other notes to serve
as a kind of "index" for your notes.

Maybe this would be a better way to see an overview of active projects and
possibly interleave other useful information that wouldn't be possible in a
plain folder structure?

What is your opinion about this?  Should I make a video on this topic?

== Join the March iteration of Hands-On Guile Scheme for Beginners! ==

In February, I announced the first iteration of the "Hands-On Guile Scheme for
Beginners" course to create a fun environment to teach the fundamentals of
Scheme, Guile, and functional programming.  We had a great time!  I also learned
a lot from the experience.

Starting March 16th, I will be running another iteration of the course with some
big improvements over last time:

- The course is now 8 weeks long to give more time for learning and exercises
- Course material will be delivered as on-demand videos and written content so
  that you can learn at your own pace
- We will have 3 live, 1-hour group Q&A sessions so that you can ask questions
  about the material or anything else related to Guile and Scheme
- You'll receive a lot of useful material at the end of the course to give you
  a clear path for what projects you can build next with Guile!

If you're interested to join, head over to the official site to see full details
about the course and how it will operate:

  -> https://systemcrafters.net/courses/hands-on-guile-scheme-beginners/

Please feel free to reply directly to this e-mail if you have *any* questions
about the course, I will be happy to answer them!

Also, if you register before *6 AM UTC on Monday March 4th*, you'll receive a
15% discount!

Hope to see you there!

== Crafter News ==

Here are some interesting news items in the broader sphere of system crafting:

* The Spritely Institute is hiring a part-time Scheme hacker!

  This is an exciting opportunity for Scheme hackers: integrating Guile Hoot and
  Spritely Goblins!

    -> https://spritely.institute/jobs/Contract-WASM-integration.html

  If you are open to short term contract positions, I would highly recommend
  checking this out.  I imagine it will get filled quickly!

* The Yeetube package has been gaining attention

  There have been a couple of recent blog posts talking about community member
  Thanos Apollo's Yeetube package:

    -> https://thanosapollo.org/post/yeetube/
    -> https://michal.sapka.me/emacs/watching-youtube-with-emacs/
    -> https://irreal.org/blog/?p=12007

  This package looks like a great way to search YouTube for videos without being
  subject to the analytics and algorithm of the platform.  Check it out!

== Friday's Stream - Crafting a Minimal Writing Environment in Emacs ==

In this week's stream, we'll experiment with a few Emacs packages like Fontaine,
Logos, Spacious Padding, TMR, and wc-mode to craft a minimal and focused writing
environment.

The stream will go live Friday, March 1st at 6PM EET (UTC+2),
  check your local time on this page: https://time.is/compare/1800_in_Athens

You can find the stream at these URLs:

  -> https://youtube.com/live/yY-qyCYc5GU
  -> https://twitch.tv/SystemCrafters
  -> https://systemcrafters.net/live-streams/march-1-2023/

== Closing ==

I hope you enjoyed this issue of the System Crafters Newsletter!  I always want
to improve and streamline the content as time goes on so please send me your
thoughts and feedback by replying directly to this e-mail!

Until next time, Happy Hacking!

        ---------------------------------------------------------------
        --    If you enjoyed this newsletter and other content by    --
        --       System Crafters, consider supporting my work!       --
        --            More information can be found here:            --
        --  https://systemcrafters.net/how-to-help/#support-my-work  --
        ---------------------------------------------------------------

--
David Wilson
david@systemcrafters.net
